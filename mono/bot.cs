using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class Bot {

	//Speed difference (between current and target) needed to ignore the need to change lanes
	double OVERRIDE_FOR_THROTTLE_DISPARITY = 10;
	//Normally when velocity is below target throttle goes to 1.0 and to 0.0 when not. This threshold will set throttle to target if current velocity is close
	double VELOCITY_FEATHERING_THRESHOLD = .1;
	//Not sure if this is correct...simple physics?
	public static double ANGLE_RATIO_VELOCITY_CEFFICIENT = 15.5; // track radius/angle * this = Vlocity to cause a crash
	//Need some kind of guess to target speed before we know the max accel/decel of the car
	public static double UNANALYZED_TRACK_SPEED = 8; ////8;//9.35 is best on OG map;  //Germany 8 is best so far
//	int turnCount = 0; 
	private TrackManager trackManager;
//	private CarManager carManager; 
	double currentVelocity;
	double previousPosition; 
	double previousVelocity = 0; 
	double lastAngle = 0; 
	List<double> decelValues;

	bool changedLanesThisLap =false;
	int lastLap = 0; 
	int lastPos = -2; 
	double lastThrottle = -1; 
	double? pieceEntryVelocity = 0; 
	double? currentPieceAngle = 0; 
	double? currentPieceRadius = 0; 
	double previousDistanceRemaining = 0; 
	int currentLaneIndex = 0; 
	int targetLaneIndex = 0;
	double lastLaneOffset = 0;
	bool checkForLaneChange = false; 
	bool currentlyChangingLane = false; 
	bool laneChangeRequested = false; 
	double? avgDecel;
	bool needDecelValues;
	bool turboAvailable = false; 
//	double turboDuration = 0; 
///	double turboFactor = 1; 
//	bool hasUsedTurbo = false; //potential way to get 2 turbos in 1 lap
	string myColor = "";
	bool inTurbo = false; 
	bool raceStarted = false;
	int dangerTest = 0; 
//	bool attemptPassing = false; 
	bool useTurbo = false;
	bool saveTurbo = false; 
	int turboResidual = 0; 

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}
	

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;
		trackManager = new TrackManager (); 

	
		//carManager = new CarManager (); 
		Join2 joinUsa = new Join2 (join.name, join.key, "usa", 1); 
		Join2 joinPentag = new Join2 (join.name, join.key, "pentag", 1); 

		Join2 joinGermany = new Join2 (join.name, join.key, "germany", 1 ); 
		Join2 joinFrance = new Join2 (join.name, join.key, "france", 1); 
		Join2 joinEngland = new Join2 (join.name, join.key, "england", 1); 
		Join2 joinJapan = new Join2 (join.name, join.key, "suzuka", 1 ); 
		Join2 joinItaly = new Join2 (join.name, join.key, "imola", 1); 
		Join2 joinEla = new Join2 (join.name, join.key, "elaeintarha", 1);

		JoinRace joinRandom = new JoinRace (join.name, join.key, 3);  
		/*
		string race = "usa"; 



		switch (race) {
		case "regular":
			send (join);
			break; 
		case "germany":
			send(joinGermany);
			break; 
		case "france":
			send (joinFrance); 
			break; 
		case "usa":
			send(joinUsa); 
			break; 
		case "england":
			send(joinEngland);
			break; 
		case "japan":
			send (joinJapan); 
			break; 
		case "italy":
			send(joinItaly); 
			break; 
		case "ela":
			send(joinEla); 
			break; 
		case "pentag":
			send(joinPentag); 
			break; 
		case "duel":
			send (joinRandom); 
			break; 
		}

*/
		//REGULAR JOIN!
		//
		send (join);


		needDecelValues = true; 
		decelValues = new List<double> (); 
		List<CarPositionData> carPosData;
		CrashRootObject crashRootData;
		SpawnRootObject spawnRootData;
		TurboEndRootObject turboEndRootData;
		TurboStartRootObject turboStartRootData;
		RootTrackObject trackRootData; 
		EOR_Root endOfRaceData;
		TurboData turboData; 
		MyCarRootObject myCarData = null;
		RootCarPosObject test;

		CarPositionData carPos;
	   // List<
		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);


			if(msg.data != null)
			{
				//Console.WriteLine(msg.data.ToString() + "\n--\n");
			
			}
			switch(msg.msgType) {
				case "carPositions":
					
					if(raceStarted == false)
					{
						send(new Ping()); 
						break; 
					}
					test = JsonConvert.DeserializeObject<RootCarPosObject>(line);
					carPosData = test.data;
					carPos = carPosData[0];
					foreach(CarPositionData currentCarPos in carPosData)
					{
						if(myColor == currentCarPos.id.color)
						{
							carPos = currentCarPos; 
						}
						//Detect CarPositions and velocity
						//REMOVE THIS!
						//carManager.UpdateCarPos(currentCarPos.id.color, currentCarPos); 
					}
					

					PositionInfo positionInfo = carPos.piecePosition;
					
					Piece currentTrackPiece = trackManager.currentTrack.pieces[positionInfo.pieceIndex];
					Piece nextTrackPiece = trackManager.currentTrack.pieces[((positionInfo.pieceIndex+1) % (trackManager.currentTrack.pieces.Count - 1))];	
					
					double throttleAmount = 0;
					
					double trackPieceLength;
					if(currentTrackPiece.radius.HasValue) //Curved Piece
					{
						double laneOffset = trackManager.currentTrack.lanes[carPos.piecePosition.lane.endLaneIndex].distanceFromCenter;
						if(currentTrackPiece.angle > 0) //Track curves to the right
						{
							laneOffset = -laneOffset; //Inside curve distance is positive to the right, but this curve angles to the right
						}
						trackPieceLength = ((int)currentTrackPiece.radius - laneOffset) * HWOUtilities.DegreeToRad(Math.Abs((int)currentTrackPiece.angle)); 
						lastLaneOffset = laneOffset;

					}else
					{
						trackPieceLength =  currentTrackPiece.length;
						lastLaneOffset = 0;
					}

				//HACK : using Math.Abs because in some cases Im getting that I am beyond the length of the track. Bug? On my side or theirs? In any case, it's always right at the end of a piece so 
				//having a tiny value is probably correct. :P This may cause a bug when determining engine power...I should check for that. 
					double currentDistanceRemaining = Math.Abs(trackPieceLength - positionInfo.inPieceDistance); 
					currentLaneIndex = carPos.piecePosition.lane.endLaneIndex;
	
				    currentVelocity = (lastPos == positionInfo.pieceIndex ? //Same track Piece as last time?
				                      (positionInfo.inPieceDistance - previousPosition) : //else
				                      (positionInfo.inPieceDistance + previousDistanceRemaining)); //current track distance so far plus whatever remained last time

					

					if(lastThrottle == 0 && needDecelValues && (trackPieceLength - positionInfo.inPieceDistance > 0)) //Weird check, but have to be sure data is valid.
					{
						if(currentVelocity < previousVelocity)
						{
							decelValues.Add(currentVelocity - previousVelocity);
						}
						if(decelValues.Count > 14)
						{
							DetermineAvgDecel(); 
							needDecelValues = false; 
						}
					}


					if(trackManager.trackAnalyzed == false && avgDecel.HasValue)
					{
					//Console.WriteLine("avgDecel : " + avgDecel); 
					//HACK! Sometimes I get crazy values here...
					if(avgDecel > .3)
					{
						avgDecel = .15; //
					}
						trackManager.AnalyzeTrack((double)avgDecel, true);
					}

					double normalizedDistanceRemaining = currentDistanceRemaining / trackPieceLength;
					double targetVelocity = 0;
					targetVelocity = HWOUtilities.Lerp(trackManager.targetVelocity[positionInfo.pieceIndex], 
				                                       trackManager.targetVelocity[((positionInfo.pieceIndex + 1) % (trackManager.currentTrack.pieces.Count - 1))],
				                                   (1 - normalizedDistanceRemaining));
					


					bool justChangedLanes = false; 

					if(lastPos != positionInfo.pieceIndex)
					{

						if(turboResidual > 0)
						{
							turboResidual--;
						}
						currentPieceAngle = (currentTrackPiece.angle); 
						currentPieceRadius = (currentTrackPiece.radius); 
						pieceEntryVelocity = currentVelocity;
						if(positionInfo.lap > 0 || positionInfo.pieceIndex > 1)
						{
							checkForLaneChange = true;
						}
					}	
					if(carPos.piecePosition.lane.startLaneIndex != carPos.piecePosition.lane.endLaneIndex)
				   	{
					//Console.WriteLine("Currently changing lanes"); 
						currentlyChangingLane = true;
						changedLanesThisLap = true;
					}else //Else we are in the same lane as the target
					if(currentlyChangingLane)
					{
					//Console.WriteLine("Finished Changing Lanes"); 
						currentlyChangingLane = false; 
						laneChangeRequested = false; 
						checkForLaneChange = true;
						justChangedLanes = true; 

			
					}

					if(checkForLaneChange && laneChangeRequested == false){
						//Determine if I should change lanes
					    targetLaneIndex = trackManager.NextTurnIsRight(positionInfo.pieceIndex, 
					                                               (currentLaneIndex == (trackManager.currentTrack.lanes.Count - 1)),
					                                                turboAvailable) ? trackManager.currentTrack.lanes.Count - 1 : 0;

						//Check for passing
						targetLaneIndex = ShouldPass(carPos.piecePosition.pieceIndex) ? 
										  (targetLaneIndex == 0 ? 1 : trackManager.currentTrack.lanes.Count - 2) : 
										   targetLaneIndex;
						
						checkForLaneChange = false; 
					}

				//if(targetLaneIndex != currentLaneIndex)
				//{
				//	if(laneChangeRequested)
				//	{
				//		if(carPos.piecePosition.pieceIndex != lastPos)
				//		{
							//Console.WriteLine("Req, waiting " + carPos.piecePosition.pieceIndex); 
				//		}
				//	}
				//}
					
					//Determine which message to send

					if(Math.Abs(targetVelocity - currentVelocity) <= VELOCITY_FEATHERING_THRESHOLD)
					{
						throttleAmount = targetVelocity/10;
					}
					else if(targetVelocity < currentVelocity)
					{
						throttleAmount = 0;
					}else
					{
						throttleAmount = 1;
					}

				bool detectCrash = false; 
				if(Math.Abs(lastAngle) < Math.Abs (carPos.angle)) //Angle is increasing
				{
					double angleDiff = Math.Abs (carPos.angle) - Math.Abs (lastAngle);
					if(Math.Abs (carPos.angle) + (angleDiff * 10) > 60)
					{
						dangerTest++;
						//Console.WriteLine(lastThrottle + " : vel:" + previousVelocity + " angle: " + carPos.angle +  "    DANGER!!!! " + dangerTest); 

					}else if(dangerTest > 0)
					{
						//Console.WriteLine("Reduce Danger " + dangerTest); 
						dangerTest--;
					}
				}else if(dangerTest > 0)
				{
					//Console.WriteLine("Clear Danger " + dangerTest); 
					dangerTest = 0;
				}

				if(dangerTest > 2)
				{
					detectCrash = true; 
				//	Console.WriteLine("DetectCrash!"); 
				}




				if(detectCrash)
				{
					throttleAmount = 0;
					send (new Throttle(throttleAmount)); 
					lastThrottle = throttleAmount;
				}else if(saveTurbo && turboAvailable && trackManager.turboAllowed[carPos.piecePosition.pieceIndex] &&
					!trackManager.currentTrack.pieces[carPos.piecePosition.pieceIndex].radius.HasValue &&
				    !trackManager.currentTrack.pieces[((carPos.piecePosition.pieceIndex + 1) % trackManager.currentTrack.pieces.Count)].radius.HasValue)
				{
					saveTurbo = false;
					send (new Turbo()); 
		//			Console.WriteLine("SAVED TURBOOOO  TURBOOOOT"); 
					turboAvailable = false; 
					useTurbo = false; 
				}
			

				//Check for turbo!
				else if(turboAvailable && useTurbo && justChangedLanes && trackManager.turboAllowed[carPos.piecePosition.pieceIndex])
				{
					if(trackManager.currentTrack.pieces[carPos.piecePosition.pieceIndex].radius.HasValue || 
					   trackManager.currentTrack.pieces[((carPos.piecePosition.pieceIndex + 1) % trackManager.currentTrack.pieces.Count)].radius.HasValue)
					{
						//Console.WriteLine("Save Turbo"); 
						saveTurbo = true; 
						send(new Throttle(throttleAmount));
						lastThrottle = throttleAmount;
					}else
					{
						send (new Turbo()); 
	//					Console.WriteLine("TURBOOOOT"); 
						turboAvailable = false; 
						useTurbo = false; 
						saveTurbo = false; 
					}
				}
				else
					//Check for changing lanes first
					if(laneChangeRequested == false &&  //We havent already requested a change
					   targetLaneIndex != currentLaneIndex && //In the wrong lane
				   	   (Math.Abs(targetVelocity - currentVelocity) < OVERRIDE_FOR_THROTTLE_DISPARITY || //we dont need to slow down or speed up IMMEDIATELY
				       lastThrottle == throttleAmount)) //or we are already accellerating or braking as much as we can anyways
					{

						bool turnRight;
						if(targetLaneIndex > currentLaneIndex) //We are too far to the left?
						{
							turnRight = true; 	
						}else
						{	
							turnRight = false;
						}
					//Console.WriteLine("Request turn. " + (turnRight ? "Right" : " left")); 
					send( new LaneChange(turnRight));
						laneChangeRequested = true; 
						
					}else
					{
						send(new Throttle(throttleAmount));
						lastThrottle = throttleAmount;
					}

					if(positionInfo.lap != lastLap && lastLap > 2)
					{
						if(changedLanesThisLap == false)
						{
							currentlyChangingLane = false; 
							laneChangeRequested = false; 
							checkForLaneChange = true;
						}
						changedLanesThisLap = false; 
					}

				lastAngle = carPos.angle; //Used for crash reporting
				lastLap = positionInfo.lap;
				lastPos = positionInfo.pieceIndex;
				previousPosition = positionInfo.inPieceDistance;
				previousDistanceRemaining = currentDistanceRemaining;
				previousVelocity = currentVelocity;




					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
						break;
				case "gameInit":
					Console.WriteLine("Race init");
					
					trackRootData = JsonConvert.DeserializeObject<RootTrackObject>(line);

					trackManager.Init(trackRootData.data); 

				//	carManager.Init(trackRootData.data.race.cars, trackManager); 
			
				saveTurbo = false; 
				currentVelocity = 0;
				 previousPosition = 0; 
				 previousVelocity = 0; 
				lastAngle = 0; 

				 lastLap = 0; 
				 lastPos = -2; 
				 lastThrottle = -1; 

				 previousDistanceRemaining = 0; 
				 currentLaneIndex = 0; 
				 targetLaneIndex = 0;
				 lastLaneOffset = 0;
				 checkForLaneChange = false; 
				 currentlyChangingLane = false; 
				 laneChangeRequested = false; 
				turboResidual = 0; 
				inTurbo = false; 
				 turboAvailable = false; 

		//		 hasUsedTurbo = false; 

				 raceStarted = false;
				 dangerTest = 0; 
	//			 attemptPassing = false; 
				 useTurbo = false;
					



					send(new Ping());
					break;
				case "gameEnd":
					endOfRaceData = JsonConvert.DeserializeObject<EOR_Root>(line);
					Console.WriteLine("Race ended: Best Lap: " + endOfRaceData.data.bestLaps[0].result.millis);
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					raceStarted = true; 
					send(new Ping());
					break;
			case "crash":
					crashRootData = JsonConvert.DeserializeObject<CrashRootObject>(line); 
					if(crashRootData.data.color == myColor)
					{
						bool considerTurbo = inTurbo || (turboResidual > 0);
						trackManager.HandleMyCrash(lastPos, considerTurbo); 
						turboAvailable = false; 
						//carManager.HandleCrashData(crashRootData); 
						currentlyChangingLane = true;
					    laneChangeRequested = false;
						Console.WriteLine("\nCrash!");
						Console.WriteLine("Velocity: " + currentVelocity + "  Crash angle:" + lastAngle + "  pos:" + lastPos + "  lap:" + lastLap); 
						Console.WriteLine("EntryVelocity: " + pieceEntryVelocity + " LastThrottle " + lastThrottle + " pieceRadius: " + currentPieceRadius + "+laneOffset " + lastLaneOffset +" pieceAngle: " + currentPieceAngle + "\n\n"); 
					}
					send (new Ping()); 
					break; 
			case "spawn":
				spawnRootData = JsonConvert.DeserializeObject<SpawnRootObject>(line); 
				if(spawnRootData.data.color == myColor)
				{
		//			Console.WriteLine("Spawn!"); 
					turboAvailable = false; 
					inTurbo = false;
					useTurbo = false; 
					saveTurbo = false; 
				}
				send (new Ping()); 
				break; 
			case "yourCar":
	//			Console.WriteLine("My car info!"); 
				myCarData = JsonConvert.DeserializeObject<MyCarRootObject>(line);
				myColor = myCarData.data.color;
				//Console.WriteLine("My color is: " + myColor); 
				send(new Ping());
				break; 
			case "turboAvailable":
				Console.WriteLine("turbo available"); 
				turboData = JsonConvert.DeserializeObject<TurboRootObject>(line).data; 
			//	turboDuration = turboData.turboDurationMilliseconds;
			//	turboFactor = turboData.turboFactor;
				turboAvailable = true; 
				send(new Ping());
				break; 
			case "turboStart":
				turboStartRootData = JsonConvert.DeserializeObject<TurboStartRootObject>(line); 
				if(turboStartRootData.data.color == myColor)
				{
		//			Console.WriteLine("Turbo start "  + lastPos); 
					trackManager.lastTurboIndex = lastPos;

					inTurbo = true; 
				}
				send (new Ping()); 
				break; 
			case "turboEnd":
				turboEndRootData = JsonConvert.DeserializeObject<TurboEndRootObject>(line); 
				if(turboEndRootData.data.color == myColor)
				{
					turboResidual = 4; //wheeee hacky!
					trackManager.lastTurboEnd = lastPos; 
					inTurbo = false; 
				}
				send (new Ping()); 
				break; 
				default:
				//Console.WriteLine("msgType : " + msg.msgType); 
					send(new Ping());
					break;
			}

		}
	}

	private bool ShouldPass(int pieceIndex)
	{
		if (pieceIndex == trackManager.switchWinner - 1) {
	//		Console.WriteLine("SWIIIITCH"); 
			if(turboAvailable)
			{	
				useTurbo = true;
			}
			return true; 
		}
		return false; 
	}

	private void send(SendMsg msg) {
		//Console.WriteLine("M: " + msg.ToJson().ToString()); 
		writer.WriteLine(msg.ToJson());
	}

	private void DetermineAvgDecel()
	{
		decelValues.Sort (); 
		double total = 0;
		double count = 0; 
		for (int i = 1; i < decelValues.Count - 2; i++) {
			total += decelValues[i];
			count++;
			//Console.WriteLine("Decel " + i + ": " + decelValues[i]); 
		}
		//avgDecel = -(total / count);

		avgDecel = -(decelValues [(decelValues.Count / 2)]);

	}

}



class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		//Console.WriteLine (JsonConvert.SerializeObject (new MsgWrapper (this.MsgType (), this.MsgData ())).ToString ()); 
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}
//Received Messages
public class Id2
{
	public string name { get; set; }
	public string color { get; set; }
}

public class Lane2
{
	public int startLaneIndex { get; set; }
	public int endLaneIndex { get; set; }
}

public class PositionInfo
{
	public int pieceIndex { get; set; }
	public double inPieceDistance { get; set; }
	public Lane2 lane { get; set; }
	public int lap { get; set; }
}

public class CarPositionData
{
	public Id2 id { get; set; }
	public double angle { get; set; }
	public PositionInfo piecePosition { get; set; }
}

public class RootCarPosObject
{
	public string msgType { get; set; }
	public List<CarPositionData> data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }
}

class Join: SendMsg {
	public string name;
	public string key;
	//public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
	//	this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}


class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class LaneChange: SendMsg {
	public bool shiftingRight;
	
	public LaneChange(bool right) {
		this.shiftingRight = right;
	}
	
	protected override Object MsgData() {
		if (shiftingRight) {
			return "Right";
		} else {
			return "Left";
		}
	}
	
	protected override string MsgType() {
		return "switchLane";
	}
}
/*
public class CarManager
{
	Dictionary<string, int> raceCarIndex;
	RaceCar[] raceCars;
	TrackManager trackManager; 
	public void Init(List<Car> cars, TrackManager myTrackManager)
	{
	
		raceCarIndex = new Dictionary<string, int> ();
		raceCars = new RaceCar[cars.Count];
		int currentIndex = 0; 
		foreach (Car car in cars) {
			RaceCar newCar = new RaceCar(); 
			raceCars[currentIndex] = newCar;
			raceCars[currentIndex].myTrackVelocity = new double[myTrackManager.currentTrack.pieces.Count];
			raceCarIndex.Add(car.id.color, currentIndex);
			currentIndex++;
		}
		trackManager = myTrackManager;
	
	}

	public void UpdateCarPos(string color, CarPositionData data)
	{
		int carIndex = raceCarIndex [color];
		RaceCar currentCar = raceCars [carIndex]; 
		Piece currentTrackPiece = trackManager.currentTrack.pieces [data.piecePosition.pieceIndex];
		double laneOffset= 0;
		double diffLaneOffset = 0;
		double trackPieceLength;
		if(currentTrackPiece.radius.HasValue) //Curved Piece
		{
			diffLaneOffset = trackManager.laneMaxDistFromCenter - trackManager.currentTrack.lanes[data.piecePosition.lane.endLaneIndex].distanceFromCenter;
			//trackPieceLength = ((int)currentTrackPiece.radius + laneOffset) * HWOUtilities.DegreeToRad(Math.Abs((int)currentTrackPiece.angle)); 
			if(currentTrackPiece.angle > 0) //Track curves to the right
			{
				laneOffset = -laneOffset; //Inside curve distance is positive to the right, but this curve angles to the right
			}
			trackPieceLength = ((int)currentTrackPiece.radius + laneOffset) * HWOUtilities.DegreeToRad(Math.Abs((int)currentTrackPiece.angle)); 

			
		
		
		}else
		{
			trackPieceLength =  currentTrackPiece.length;
		}

		double currentVelocity = (currentCar.lastTrackPiece == data.piecePosition.pieceIndex ? //Same track Piece as last time?
		                   (data.piecePosition.inPieceDistance - currentCar.previousInPieceDistance) : //else
		                   (data.piecePosition.inPieceDistance + currentCar.previousDistanceRemaining)); //current track distance so far plus whatever remained last time

		currentCar.previousInPieceDistance = data.piecePosition.inPieceDistance;

		if (currentCar.lastTrackPiece != data.piecePosition.pieceIndex) {
			//Car has moved into a new track piece. Calc velocity and check it against stuff. 

			//HACK something seriously wrong with velocities when exiting curves :P
			currentVelocity = currentCar.previousVelocity;

			double adjustedVelocity = currentVelocity + (.025 * diffLaneOffset); 
			//Console.WriteLine (color + " moved to " + data.piecePosition.pieceIndex + " LastCheckin: " + currentCar.lastVelocityCheckin + " IPD: " + data.piecePosition.inPieceDistance + " : pdr: " + currentCar.previousDistanceRemaining + " NV: " +currentVelocity); 
			currentVelocity = adjustedVelocity;
			//Console.WriteLine("Lane Offset adjustment: " + (currentVelocity - adjustedVelocity)); 
			if (currentVelocity > currentCar.myTrackVelocity [data.piecePosition.pieceIndex]) {

				currentCar.myTrackVelocity [data.piecePosition.pieceIndex] = currentVelocity; 
			}

			if (currentCar.lastVelocityCheckin <= (data.piecePosition.pieceIndex +  (trackManager.targetVelocity.Length * data.piecePosition.lap)) - (RaceCar.CHECK_IN_LEAD * 2)) {
				//Update info! 
				int trackPieceIndex;

				for (int i = 0; i < RaceCar.CHECK_IN_LEAD; i++) {
					trackPieceIndex = ((currentCar.lastVelocityCheckin + i) % trackManager.currentTrack.pieces.Count);

					if (currentCar.myTrackVelocity [trackPieceIndex] >= trackManager.targetVelocity [trackPieceIndex] * .9) {
						//Console.WriteLine (trackPieceIndex + "  :Velocity of : " + trackManager.targetVelocity [trackPieceIndex] + "   replaced with: " + currentCar.myTrackVelocity [trackPieceIndex]); 

						trackManager.targetVelocity [trackPieceIndex] = currentCar.myTrackVelocity [trackPieceIndex] * .9;
					}
				}
				trackManager.AnalyzeTrack(); 
				currentCar.lastVelocityCheckin = (data.piecePosition.pieceIndex + ( trackManager.targetVelocity.Length * data.piecePosition.lap)  ) - RaceCar.CHECK_IN_LEAD;
			}
		}
		//For use next tick

		currentCar.previousDistanceRemaining = Math.Abs(trackPieceLength - data.piecePosition.inPieceDistance); 
		currentCar.lastTrackPiece = data.piecePosition.pieceIndex;
		
		currentCar.lap = data.piecePosition.lap;

		//TODO:
		//Add in some kind of angle-velocity check to keep from crashing with bad data...in the same way, if you detect too LOW of a slip angle, increase the speed a bit for the next lap
		//Add turbo code
		//add passing code

		//WOW what a hack!
		currentCar.previousVelocity = currentVelocity;

	}

	public void HandleCrashData(CrashRootObject crashRootData)
	{
		RaceCar crashedCar = raceCars[raceCarIndex[crashRootData.data.color]];
		//This keeps us from checking in that last chunk of data
		crashedCar.lastVelocityCheckin = crashedCar.lastTrackPiece + 1; 
	}
}
*/
public class RaceCar
{

	//Number of pieces the ca has to have behind them before checking in velocities
	public static int CHECK_IN_LEAD = 5;
	public double lap;
	public int lastTrackPiece; 
	public double lastAngle; 
	public double previousDistanceRemaining;
	//public double[] myTrackVelocity; 
	public double previousVelocity;
	public double previousInPieceDistance;
	public int lastVelocityCheckin;
	public RaceCar()
	{
		previousInPieceDistance = 0; 
		previousVelocity = 0; 
		lap = -1; 
		lastTrackPiece = -1;
		previousDistanceRemaining = 0;
		lastAngle = 0; 
		lastVelocityCheckin = 0; 

	}
}




public class TrackManager{
	public Track currentTrack;
	public List<Car_> cars;
	public double[] targetVelocity;  
	
	public double[] slipAngleAtSafe;
	public double laneMaxDistFromCenter; 

	public bool[] turboAllowed;
	public int lastTurboEnd = -1; 
	public bool trackAnalyzed; 
	public double? storedMaxDecel;

	public int lastTurboIndex = -1;
	public int switchWinner = -1;

	public void Init(TrackData trackData)
	{
		if (trackData == null) {
			Console.WriteLine ("NULL! ABORT!"); 
		} else {
			currentTrack = trackData.race.track;
		}
		trackAnalyzed = false; 
		targetVelocity = new double[currentTrack.pieces.Count];
		turboAllowed = new bool[currentTrack.pieces.Count];
		for(int i = 0; i < currentTrack.pieces.Count; i++) {
		targetVelocity[i] = DetermineTrackPieceVelocity(currentTrack.pieces[i]); 
			turboAllowed[i] = true; 
		/*
			Console.WriteLine("TP:" + i + 
			                  " len:" + currentTrack.pieces[i].length +
			                  " rad:" + currentTrack.pieces[i].radius + 
			                  " ang:" + currentTrack.pieces[i].angle + 
			                  " swt:" + currentTrack.pieces[i].@switch +
			                  " TVel:" + targetVelocity[i]); 
			
		*/	
		//	Console.WriteLine("T["+i+"]:"+targetVelocity[i]); 
		}
		laneMaxDistFromCenter = 0; 
		foreach (Lane lane in currentTrack.lanes) {
			if(lane.distanceFromCenter > laneMaxDistFromCenter)
			{
				laneMaxDistFromCenter = lane.distanceFromCenter;
			}
		}
	

		//foreach (CarInfo carInfo in trackData.race.cars) {
		//	Car newCar = new Car(); 
		//	newCar.name = carInfo.id.name;
		//}

		

	}

	public void DisplayTrackData()
	{

		for (int i = 0; i < currentTrack.pieces.Count; i++) {

		Console.WriteLine("T["+i+"]:"+targetVelocity[i]); 
		}
	}

	public void HandleMyCrash(int myLastPosition, bool inTurbo)
	{
		//Turn down the velocities on the last 2 track pieces
		///	DisplayTrackData (); 
		if (inTurbo) {
			//Console.WriteLine("crashed In turbo"); 
			if(lastTurboIndex != -1)
			{
				int currentIndex = lastTurboIndex;
				int trueIndex; 
				do{
					trueIndex = (currentIndex % currentTrack.pieces.Count);
				//	Console.WriteLine("No turbo at: " + trueIndex); 
					turboAllowed[trueIndex] = false; 
					currentIndex++;
				}while(trueIndex != myLastPosition);


			}
		}
		targetVelocity [myLastPosition] *= .8;
		targetVelocity [((myLastPosition + targetVelocity.Length - 1) % targetVelocity.Length)] *= .7;
		AnalyzeTrack (); 
		//Console.WriteLine ("Adjusted " + myLastPosition + " : and " + (myLastPosition + targetVelocity.Length - 1) % targetVelocity.Length);

	}

	public void AnalyzeTrack(double maxDecel = -1, bool findPassingLanes = false)
	{
		if (maxDecel == -1) {
			if(storedMaxDecel.HasValue)
			{
				maxDecel = (double)storedMaxDecel; 
			}else
			{
				return;
			}
		} else {
			storedMaxDecel = maxDecel;
		}
		//	Console.WriteLine ("calculated avgDecl:" + maxDecel); 
		//TODO: Set all straight pieces trackvelocity to 10! Otherwise we hit that limit when iterating backwards as we dont know if its a valid limit or not!
		for (int index = 0; index < currentTrack.pieces.Count; index++) {
			if (currentTrack.pieces [index].radius.HasValue == false) {
				targetVelocity [index] = 100.0;
			}
		}

		//Go forward and find the first curve
		int i = 0; 
		do {
			if (currentTrack.pieces [i].radius.HasValue) {
				break;
			}
			i++;
		} while(i < currentTrack.pieces.Count);

		//Work backwards around the track and adjust the entry position based on the NEXT piece
		int count = 0;
		int currentTrackIndex;
		int nextTrackIndex;
		double currentMaxEntryVelocity; 
		while(count < currentTrack.pieces.Count)
		{
			//Maximum entry velocity (targetVelocity) is either determined by the curve or by the NEXT pieces value adjusted for distance of THIS piece and braking speed
			if(i == -1)
			{
				i = currentTrack.pieces.Count - 1;
			}
			currentTrackIndex = (i % currentTrack.pieces.Count);
	//		Console.Write("currentTP: " + currentTrackIndex);

			nextTrackIndex = ((i + 1) % currentTrack.pieces.Count); 
			//Console.Write("-"+ currentTrackIndex +"-"); 
			if(currentTrack.pieces[currentTrackIndex].radius.HasValue)
			{
				currentMaxEntryVelocity = targetVelocity[currentTrackIndex];
			}else
			{
				currentMaxEntryVelocity = 100.0;
			}
			currentMaxEntryVelocity = Math.Min (currentMaxEntryVelocity, GetMaxEntryVelocityAdjustedForBraking(currentTrack.pieces[currentTrackIndex],
			                                                                                                   currentTrack.pieces[nextTrackIndex],
			                                                                                                   currentTrackIndex,
			                                                                                                   maxDecel)); 
			targetVelocity[currentTrackIndex] = currentMaxEntryVelocity;
			i--;

			if(count++ >= currentTrack.pieces.Count)
			{
				//lastly - check the first one we checked again to be sure we didnt start by looking at a TINY curv leading into a GIANT curve. 
				//After the first pass, the GIANT curve will be updated and we will need to iterate through again to smooth it out. 
				//??? How do I do this?
			}

		}

		trackAnalyzed = true; 
		//Console.WriteLine ("MAX DEC: " + maxDecel); 
		//for(i= 0; i < currentTrack.pieces.Count; i++) {
			//Console.WriteLine("T["+i+"]:"+targetVelocity[i]); 
		//}


		//Console.WriteLine ("Check Passing"); 

		if (findPassingLanes) {
			List<int> passingIndexes = new List<int>();
			double currentTotalAngle = 0;
			double minAngleSwitch = 100000;
			
			for(i = 0; i< currentTrack.pieces.Count; i++)
			{
				if(currentTrack.pieces[i].@switch.HasValue && currentTrack.pieces[i].@switch == true)
				{
					passingIndexes.Add(i); 
				}
			}


			for(int j = 0; j < passingIndexes.Count; j++)
			{
				currentTotalAngle = 0;
				//Console.WriteLine ("J = " + passingIndexes[j]); 
				for(int currentIndex = passingIndexes[j]; (currentIndex % currentTrack.pieces.Count) != passingIndexes[((j+1)%passingIndexes.Count)]; currentIndex++)
				{
					if(currentTrack.pieces[(currentIndex % currentTrack.pieces.Count)].angle.HasValue)
					{
						currentTotalAngle += Math.Abs((double)currentTrack.pieces[(currentIndex % currentTrack.pieces.Count)].angle);
					//	Console.WriteLine("Adding from index : " + (currentIndex % currentTrack.pieces.Count) + " : "
					//		+  Math.Abs((double)currentTrack.pieces[(currentIndex % currentTrack.pieces.Count)].angle)); 

					}
				}
				//Console.WriteLine("CTA: " + currentTotalAngle); 
				if(currentTotalAngle < minAngleSwitch)
		        {
					minAngleSwitch = currentTotalAngle;
					switchWinner = passingIndexes[j];
				}
			}
			//Console.WriteLine("Passing index = " + switchWinner); 
		}

	}

	double GetMaxEntryVelocityAdjustedForBraking(Piece thisPiece, Piece nextPiece, int thisPieceIndex, double maxDecel)
	{
		//Determine length of this piece. 
		//TODO: What do I do about the lane? Do I modify these values on the fly when reading them in based on which lane Im in?
		//Until then, Im going to use the center :P
		double trackPieceLength;
		if(thisPiece.radius.HasValue) // Piece
		{
			trackPieceLength = ((int)thisPiece.radius) * HWOUtilities.DegreeToRad(Math.Abs((int)thisPiece.angle)); 
		}else
		{
			trackPieceLength =  thisPiece.length;
		}

		//Check entry speed to next piece and determine this entry speed based on distance and decel

		double speed = targetVelocity [(thisPieceIndex + 1) % currentTrack.pieces.Count];
		double distance = trackPieceLength;
		do {
			speed += maxDecel;
			distance -= speed;
		
		} while((distance - (speed + maxDecel)) > 0);

		double trueEntrySpeed;
		if (distance > 0) {
			//We have a remainder. Lerp between last speed and this speed based ont he distance remaining.
			double nextSpeed = speed + maxDecel; 
			double lerpAmount = distance / nextSpeed;
	 
			trueEntrySpeed = HWOUtilities.Lerp (speed, nextSpeed, lerpAmount); 

		} else {
			trueEntrySpeed = speed; 
		}

		return trueEntrySpeed;
	
	}

	double DetermineTrackPieceVelocity(Piece trackPiece)
	{
		if (trackPiece.radius.HasValue) {
			//this is where some of the magic happens. 

			double initialSpeed = (((1.1 - ((double)(Math.Abs((double)trackPiece.angle) / (double)trackPiece.radius)))) * 10.65) -.5;
			double secondaryAddition = 2.75 * ((double)(Math.Abs((double)trackPiece.angle/(double)trackPiece.radius)));
			double tertiaryAdjustment = .5;

			double finalSpeed = (initialSpeed + secondaryAddition) - tertiaryAdjustment;
			//Console.WriteLine("Rad: " + trackPiece.radius + "   ang: " + trackPiece.angle + "  calc : " + initialSpeed + " : " + secondaryAddition + " : " + finalSpeed);  
			return finalSpeed;//(((1.1 - ((double)(Math.Abs((double)trackPiece.angle) / (double)trackPiece.radius)))) * 10.65) -.5 ;  //was 10.65 ) -.5;
		} else {
			return Bot.UNANALYZED_TRACK_SPEED;//8;//9.35 is best on OG map;  //Germany 8 is best so far
		}
	}
	public bool NextTurnIsRight(int startingTrackIndex, bool inRightLane, bool hasTurbo)
	{
		int trueIndex;
		int count = 0; 
		bool foundSwitch = false; 
		double totalRightDistance = 0; 

		while(count < currentTrack.pieces.Count)
		{
			startingTrackIndex++; //The starting track should be one car is currently ON, so we ginore it

			trueIndex = ((startingTrackIndex) % (currentTrack.pieces.Count - 1));
			if(currentTrack.pieces[trueIndex].@switch.HasValue)
			{
				if(currentTrack.pieces[trueIndex].@switch == true)
				{
					if(foundSwitch && totalRightDistance != 0)
					{
						//Return true if there is more right-curve distance than left-curve distance
						return (totalRightDistance > 0);
					}else
					{
						if(trueIndex == switchWinner ) //&& hasTurbo) // lets do this regardless of turbo...just for passing in general
						{
							//Console.WriteLine("Ignore switch for switchWinner!"); 
							//Stay in current lane until switchwinner comes up!!!
							if(inRightLane)
							{
								return true; 
							}else
							{
								return false;
							}
						}
						foundSwitch = true;
						continue; 
					}

				}
			}
			if(foundSwitch && currentTrack.pieces[trueIndex].angle.HasValue)
			{
				//Keep track of the biggest curve(s) in between the upcoming switch and the one after it
				totalRightDistance += (int)currentTrack.pieces[trueIndex].radius * HWOUtilities.DegreeToRad((int)currentTrack.pieces[trueIndex].angle);
			}

			count++; //Just In Case!
		}
		Console.WriteLine ("No curves found over the entire track!"); 
		return false; 
	}
}
	
	public class Car_
	{
		public string name; 
		public float angle; 
		public Lane lane; 
		public int lap; 
		public Dimensions dimensions; 
		
	}
	
	
public class Piece
{
	public double length { get; set; }
	public bool? @switch { get; set; }
	public int? radius { get; set; }
	public double? angle { get; set; }
}

public class Lane
{
	public int distanceFromCenter { get; set; }
	public int index { get; set; }
}

public class Position
{
	public double x { get; set; }
	public double y { get; set; }
}

public class StartingPoint
{
	public Position position { get; set; }
	public double angle { get; set; }
}

public class Track
{
	public string id { get; set; }
	public string name { get; set; }
	public List<Piece> pieces { get; set; }
	public List<Lane> lanes { get; set; }
	public StartingPoint startingPoint { get; set; }
}

public class Id
{
	public string name { get; set; }
	public string color { get; set; }
}

public class Dimensions
{
	public double length { get; set; }
	public double width { get; set; }
	public double guideFlagPosition { get; set; }
}

public class Car
{
	public Id id { get; set; }
	public Dimensions dimensions { get; set; }
}

public class RaceSession
{
	public int laps { get; set; }
	public int maxLapTimeMs { get; set; }
	public bool quickRace { get; set; }
}

public class Race
{
	public Track track { get; set; }
	public List<Car> cars { get; set; }
	public RaceSession raceSession { get; set; }
}

public class TrackData
{
	public Race race { get; set; }
}

public class RootTrackObject
{
	public string msgType { get; set; }
	public TrackData data { get; set; }
	public string gameId { get; set; }
}

public class JoinRaceData
{
	public BotId botId { get; set; }
	public int carCount { get; set; }
}
class Turbo : SendMsg {
	public string turboMsg = "LEEERROOOOOOY JEEEENKIIIINS!";
	protected override string MsgType()
	{
		return "turbo";
	}
	protected override Object MsgData(){
		return this.turboMsg; 
	}
}


class JoinRace : SendMsg {

	public string msgType { get; set; }
	public JoinRaceData data { get; set; }
	public JoinRace(string name, string key, int carCount)
	{
		data = new JoinRaceData ();
		data.botId = new BotId (); 
		data.botId.key = key;
		data.botId.name = name; 
		data.carCount = carCount;
	}
	protected override string MsgType() { 
		return "joinRace";
	}
	
	protected override Object MsgData() {
		return this.data;
	}
}

class Join2: SendMsg {
	
	public JoinData joinData; 

	public Join2(string name, string key, string mapName, int carCount) {
		joinData = new JoinData (); 
		joinData.trackName = mapName;
		joinData.carCount = carCount;
		joinData.botId = new BotId (); 
		joinData.botId.key = key; 
		joinData.botId.name = name; 
	}
	
	protected override string MsgType() { 
		return "joinRace";
	}

	protected override Object MsgData() {
		return this.joinData;
	}

}

public class BotId
{
	public string name { get; set; }
	public string key { get; set; }
}

public class JoinData
{
	public BotId botId { get; set; }
	public string trackName { get; set; }
	public int carCount { get; set; }
}


//END OF RACE
public class EOR_Car
{
	public string name { get; set; }
	public string color { get; set; }
}

public class EOR_CarResult
{
	public int laps { get; set; }
	public int ticks { get; set; }
	public int millis { get; set; }
}

public class Result
{
	public EOR_Car car { get; set; }
	public EOR_CarResult result { get; set; }
}


public class BestLap
{
	public EOR_Car car { get; set; }
	public EOR_CarResult result { get; set; }
}

public class EOR_Data
{
	public List<Result> results { get; set; }
	public List<BestLap> bestLaps { get; set; }
}

public class EOR_Root
{
	public string msgType { get; set; }
	public EOR_Data data { get; set; }
}


public class TurboData
{
	public double turboDurationMilliseconds { get; set; }
	public int turboDurationTicks { get; set; }
	public double turboFactor { get; set; }
}

public class TurboRootObject
{
	public string msgType { get; set; }
	public TurboData data { get; set; }
}

public class TurboEndRootObject
{
	public string msgType { get; set; }
	public TurboEndData data { get; set; }
	public int gameTick { get; set; }
}

public class TurboEndData
{
	public string name { get; set; }
	public string color { get; set; }
}


public class TurboStartRootObject
{
	public string msgType { get; set; }
	public TurboStartData data { get; set; }
	public int gameTick { get; set; }
}

public class TurboStartData
{
	public string name { get; set; }
	public string color { get; set; }

	
}

public class MyCarData
{
	public string name { get; set; }
	public string color { get; set; }
}

public class MyCarRootObject
{
	public string msgType { get; set; }
	public MyCarData data { get; set; }
}

	
public class CrashData
{
	public string name { get; set; }
	public string color { get; set; }
}

public class CrashRootObject
{
	public string msgType { get; set; }
	public CrashData data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }
}


public class SpawnData
{
	public string name { get; set; }
	public string color { get; set; }
}

public class SpawnRootObject
{
	public string msgType { get; set; }
	public SpawnData data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }
}



public static class HWOUtilities
{
	public static double DegreeToRad(double? Degree)
	{
		if (Degree.HasValue) {
			return (double)Degree * 0.0174532925; 
		}
		return 0; 
	}

	//min, max, val
	public static double Lerp(double value1, double value2, double amount)
	{
		if (value1 > value2) {
			return value1 - (value1 - value2) * amount;
		} else {
			return value1 + (value2 - value1) * amount;
		}
	}
}



/*
 * 	if(absAngle > 40)
					{	
					throttleAmount = .001;
					}
					else if((currentTrackPiece.radius.HasValue == false && currentDistanceRemaining > 8) ||
				       (nextPiece.radius.HasValue == false && currentDistanceRemaining < 4))
					{
					throttleAmount = .81;
					}else
					if(absAngle > 30)
					{
					throttleAmount = 0;
					}else
					if(absAngle > 0)
					{
					throttleAmount = .01;
					}
					else
					{
						throttleAmount = .61;
					}

				//When running with this typo,the sim bot ended up running at about EXACTLY .61 unit-distances per tick. Throttle tries to tell it how many units to move per tick. 
*/



/*
Crashes:
Velocity: 6.99887446658558  Crash angle:-57.4937828001199  pos:15  lap:0
EntryVelocity: 7.05173561054016 pieceRadius: 100 pieceAngle: -45

Velocity: 6.86427153905338  Crash angle:-59.2129617471011  pos:31  lap:0
EntryVelocity: 6.86427153905338 pieceRadius: 100 pieceAngle: 45

Velocity: 7.02097682381299  Crash angle:-59.9066925279335  pos:15  lap:1
EntryVelocity: 6.87416362292763 pieceRadius: 100 pieceAngle: -45

Velocity: 7.88545892743196  Crash angle:59.7921860305301  pos:27  lap:1
EntryVelocity: 7.61296005949113 pieceRadius: 100 pieceAngle: 45

Velocity: 7.35158775822185  Crash angle:59.6228715517447  pos:27  lap:0
EntryVelocity: 7.07008365759049 pieceRadius: 100 pieceAngle: 45

Velocity: 7.35158775822185  Crash angle:59.6228715517447  pos:27  lap:0
EntryVelocity: 7.07008365759049 pieceRadius: 100 pieceAngle: 45

Velocity: 8.2583271741958  Crash angle:57.2095026995615  pos:20  lap:2
EntryVelocity: 8.14950307501061 pieceRadius: 100+laneOffset -10 pieceAngle: 45

Crash!
Velocity: 6.38369658982644  Crash angle:-58.518027267795  pos:15  lap:0
EntryVelocity: 6.98509876242488 pieceRadius: 100+laneOffset -10 pieceAngle: -45

Crash!
Velocity: 8.09217644381208  Crash angle:56.3545629956515  pos:20  lap:0
EntryVelocity: 7.75751461977619 pieceRadius: 100+laneOffset 10 pieceAngle: 45

Velocity: 8.20019559763689  Crash angle:55.9749396345188  pos:33  lap:0
EntryVelocity: 8.16346479682852 pieceRadius: 100+laneOffset 10 pieceAngle: 45

Velocity: 6.5699707571118  Crash angle:58.6180990878118  pos:5  lap:1
EntryVelocity: 7.12293742079279 pieceRadius: 100+laneOffset -10 pieceAngle: 45

Velocity: 6.38369658982644  Crash angle:-58.518027267795  pos:15  lap:0
EntryVelocity: 6.98509876242488 pieceRadius: 100+laneOffset -10 pieceAngle: -45

Velocity: 6.04990936613918  Crash angle:-59.4163790562887  pos:15  lap:0
EntryVelocity: 6.73855228533849 pieceRadius: 100+laneOffset -10 pieceAngle: -45

Velocity: 7.7020087784777  Crash angle:58.0316381131483  pos:20  lap:0
EntryVelocity: 7.76437003604467 pieceRadius: 100+laneOffset 10 pieceAngle: 45

Velocity: 7.85172145643094  Crash angle:58.6016437447865  pos:28  lap:1
EntryVelocity: 8.01196057110816 pieceRadius: +laneOffset 0 pieceAngle: 


*
*

TP:0 len:100 rad: ang: swt:
TP:1 len:100 rad: ang: swt:
TP:2 len:100 rad: ang: swt:
TP:3 len:100 rad: ang: swt:True
TP:4 len:0 rad:100 ang:45 swt:
TP:5 len:0 rad:100 ang:45 swt:
TP:6 len:0 rad:100 ang:45 swt:
TP:7 len:0 rad:100 ang:5 swt:
TP:8 len:0 rad:200 ang:22.5 swt:True
TP:9 len:100 rad: ang: swt:
TP:10 len:100 rad: ang: swt:
TP:11 len:0 rad:200 ang:-22.5 swt:
TP:12 len:100 rad: ang: swt:
TP:13 len:100 rad: ang: swt:True
TP:14 len:0 rad:100 ang:-45 swt:
TP:15 len:0 rad:100 ang:-45 swt:
TP:16 len:0 rad:100 ang:-45 swt:
TP:17 len:0 rad:100 ang:-45 swt:
TP:18 len:100 rad: ang: swt:True
TP:19 len:0 rad:100 ang:45 swt:
TP:20 len:0 rad:100 ang:45 swt:
TP:21 len:0 rad:100 ang:45 swt:
TP:22 len:0 rad:100 ang:45 swt:
TP:23 len:0 rad:200 ang:22.5 swt:
TP:24 len:0 rad:200 ang:-22.5 swt:
TP:25 len:100 rad: ang: swt:True
TP:26 len:0 rad:100 ang:45 swt:
TP:27 len:0 rad:100 ang:45 swt:
TP:28 len:62 rad: ang: swt:
TP:29 len:0 rad:100 ang:-45 swt:True
TP:30 len:0 rad:100 ang:-45 swt:
TP:31 len:0 rad:100 ang:45 swt:
TP:32 len:0 rad:100 ang:45 swt:
TP:33 len:0 rad:100 ang:45 swt:
TP:34 len:0 rad:100 ang:45 swt:
TP:35 len:100 rad: ang: swt:True
TP:36 len:100 rad: ang: swt:
TP:37 len:100 rad: ang: swt:
TP:38 len:100 rad: ang: swt:
TP:39 len:90 rad: ang: swt:


 *
*
*GERMANY
*
*Velocity: 4.29134454098698  Crash angle:58.2433300690055  pos:25  lap:2
EntryVelocity: 4.641186838703 pieceRadius: 50+laneOffset -10 pieceAngle: 45

Velocity: 4.72826833590184  Crash angle:59.4882531462542  pos:4  lap:2
EntryVelocity: 4.96121971087195 pieceRadius: 50+laneOffset -10 pieceAngle: 45

Velocity: 4.45372038261951  Crash angle:59.762321043568  pos:25  lap:1
EntryVelocity: 4.74440543297635 pieceRadius: 50+laneOffset -10 pieceAngle: 45


Velocity: 4.40815295247248  Crash angle:57.6090154308099  pos:4  lap:1
EntryVelocity: 4.69500278431269 pieceRadius: 50+laneOffset -10 pieceAngle: 45

*
*
*/
